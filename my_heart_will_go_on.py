import os
import csv

# automaticly install and load numpy and scipy if it is not present on the machine
try:
    import numpy as np
except ImportError:
    os.system('python -m pip install numpy')
    import numpy as np

try:
    import scipy.stats as stats
except ImportError:
    os.system('python -m pip install scipy')
    import scipy.stats as stats

# location of the data to use
data_location = "train.csv"
# location of the test data
test_location = "test.csv"
# location of the results
result_location = "result.csv"

# load the data into numpy
data1 = np.genfromtxt(data_location, delimiter=',',names=["PassengerId","Survived","Pclass"], dtype=None, encoding=None, usecols=np.arange(0,3))
# import PassengerID
PassengerId = np.genfromtxt(data_location, delimiter=',',names=["PassengerId"],skip_header=1, dtype=None, encoding=None, usecols=np.arange(0,1))
# import Survival data
Survived = np.genfromtxt(data_location, delimiter=',',names=["Survived"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(1,2))
# import Passenger Class data
Pclass = np.genfromtxt(data_location, delimiter=',',names=["Pclass"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(2,3))
# import Sex data and dummy code Sex as 0 for male and 1 for female
Sex = np.genfromtxt(data_location, delimiter=',',names=["Sex"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(5,6))
for item in Sex:
    if str(item[0]).startswith("m"):
        item[0] = 0
    if str(item[0]).startswith("f"):
        item[0] = 1
# import Age and set missing age data to -99
Age = np.genfromtxt(data_location, delimiter=',',names=["Age"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(6,7))
for item in Age:
    if "nan" == str(item[0]):
        item[0] = -99
# import sibiling data
SibSp = np.genfromtxt(data_location, delimiter=',',names=["SibSp"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(7,8))
# import parent data
Parch = np.genfromtxt(data_location, delimiter=',',names=["Parch"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(8,9))
# import Fare data
Fare = np.genfromtxt(data_location, delimiter=',',names=["Fare"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(10,11))
# import Embarked data.
Embarked = np.genfromtxt(data_location, delimiter=',',names=["Embarked"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(12,13))
for item in Embarked:
    if "" == str(item[0]):
        item[0] = "Z"

# calculate Kendall Tau for various factors
print()
print("Sex")
sex_tau, p_value = stats.kendalltau(Survived,Sex)
print(sex_tau)
print(p_value)

print()
print("Age")
age_tau, p_value = stats.kendalltau(Survived,Age)
print(age_tau)
print(p_value)

print()
print("SibSp")
sibsp_tau, p_value = stats.kendalltau(Survived,SibSp)
print(sibsp_tau)
print(p_value)

print()
print("Parch")
parch_tau, p_value = stats.kendalltau(Survived,Parch)
print(parch_tau)
print(p_value)

print()
print("Fare")
fare_tau, p_value = stats.kendalltau(Survived,Fare)
print(fare_tau)
print(p_value)

# This function is used to determine if the person survived. A score is calculated based on a number of factors.
# If the score is equal to or above the cutoff, then the person is said to survive
# The function will return 1 for survival and 0 for death.
def binary_survival_score_model(score_cutoff,in_sex, in_sex_tau, in_age, in_age_tau, in_sibsp, in_sibsp_tau, in_parch, in_parch_tau, in_fare, in_fare_tau):
    score = (in_sex_tau*100*in_sex[0]) + (in_age_tau*100*in_age[0]) + (in_sibsp_tau*100*in_sibsp[0]) + (in_parch_tau*100*in_parch[0]) + (in_fare_tau*100*in_fare[0])
    # print(score)
    # if the calculated score is less then the cutoff,
    # then we return 0 to indicate a prediction of death
    if score < score_cutoff:
        return 0, score, score_cutoff

    # if the calculated score is equal to or greater than the cutoff,
    # then we return 1 to indicate a prediction of survival
    if score >= score_cutoff:
        return 1, score, score_cutoff

    # return -99 to indicate error
    return -99999, -99999, -99999


# load the test data into numpy
# import PassengerID
PassengerId = np.genfromtxt(test_location, delimiter=',',names=["PassengerId"],skip_header=1, dtype=None, encoding=None, usecols=np.arange(0,1))
# import Passenger Class data
Pclass = np.genfromtxt(test_location, delimiter=',',names=["Pclass"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(1,2))
# import Sex data and dummy code Sex as 0 for male and 1 for female
Sex = np.genfromtxt(test_location, delimiter=',',names=["Sex"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(4,5))
for item in Sex:
    if str(item[0]).startswith("m"):
        item[0] = 0
    if str(item[0]).startswith("f"):
        item[0] = 1
# import Age and set missing age data to -99
Age = np.genfromtxt(test_location, delimiter=',',names=["Age"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(5,6))
for item in Age:
    if "nan" == str(item[0]):
        item[0] = -99
# import sibiling data
SibSp = np.genfromtxt(test_location, delimiter=',',names=["SibSp"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(6,7))
# import parent data
Parch = np.genfromtxt(test_location, delimiter=',',names=["Parch"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(7,8))
# import Fare data
Fare = np.genfromtxt(test_location, delimiter=',',names=["Fare"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(9,10))
# import Embarked data.
Embarked = np.genfromtxt(test_location, delimiter=',',names=["Embarked"], skip_header=1, dtype=None, encoding=None, usecols=np.arange(10,11))
for item in Embarked:
    if "" == str(item[0]):
        item[0] = "Z"

# Create csv file for output
with open(result_location, 'w',newline='') as csv_output_file:
    output = csv.writer(csv_output_file, delimiter=',', quotechar=' ', quoting=csv.QUOTE_MINIMAL)
    output.writerow(["Jay Kinzie,COMP479 Fall 2019,Homework 1 - Titanic Data Output"])
    output.writerow(["index,PassengerId,Predicted Survival, Passenger Survival Score, Passenger Survival Score Cutoff"])

    i = PassengerId.size-1
    # print(i)
    while 0 < i:
        # function to calculate probability of survival
        survival, passenger_score, passenger_score_cutoff = binary_survival_score_model(1000,
                                               np.frombuffer( Sex[i - 1]), sex_tau,
                                               np.frombuffer( Age[i - 1]), age_tau,
                                               SibSp[i - 1], sibsp_tau,
                                               Parch[i - 1], parch_tau,
                                               Fare[i - 1], fare_tau)

        output.writerow([i,str(PassengerId[i-1]).strip('(').strip(')').strip(","),survival, passenger_score, passenger_score_cutoff])
        i = i-1








